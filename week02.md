# Week 02

Still not getting the hang of the mouse control at this point.

Sam and I have decided to make an Airsoft type Shooter game with 360 video of an Airsoft game in the cut scene between levels.

Have worked on a basic 360 video player. I create an Inverted Sphere, place the camera inside the sphere and give the spheres inside a texture of a 360 video I have previously recorded.

Have created a Scene/level on which I have added some objects, some character models, and have given them a basic AI using an Asset package called RAIN. This basically involves creating a navigation mesh, of where you want things to be able to walk, a navigation waypath for the AI to follow. At this point I do not have animation working, the charatacters just move around.

I have also used a small bit of C# to make a script to control the camera. 


Code:
```cs
using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour
{
	public float speed = 0.1f;
	void Update()
	{
		if(Input.GetKey(KeyCode.RightArrow))
		{
			transform.Translate(new Vector3(speed * Time.deltaTime,0,0));
		}
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			transform.Translate(new Vector3(-speed * Time.deltaTime,0,0));
		}
		if(Input.GetKey(KeyCode.DownArrow))
		{
			transform.Translate(new Vector3(0,0,-speed * Time.deltaTime));
		}
		if(Input.GetKey(KeyCode.UpArrow))
		{
			transform.Translate(new Vector3(0,0,speed * Time.deltaTime));
		}
	}
}
```

The hardest bit is developing without the HTC vive, as I cannot test my code with it. I found when I came in, i couldnt get the scene to loiad through the vive. So there has been some dificulty there.