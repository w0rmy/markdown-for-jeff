# Week 03

Have put a lot of time into looking at NPC AI. Have found that most tasks needed within Unity, can be achieved via Asset packages, code and images that others have made and shared. I have installed RAIN UI into my scene and have got basic movement working on my NPC characters. The movement is not animated yet, thats another step, but I am happily starting to understand the concepts around your meshes.

Made a big commitment to this project yesterday. I purchased a new PC and a Vive to allow my developement to continue more at home.

This week with jacob, we have been using the VRTK to allow to picking up of objects.

Purchased a VIVE for at home,so have unlimited access to test now.

Added script to change from one level to another based on a time limit

Code:
```cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextLevel : MonoBehaviour {

	public float delay = 15;
	public string NewLevelString = "Bootcamp";
	void Start()
	{
		StartCoroutine(LoadLevelAfterDelay(delay));     
	}

	IEnumerator LoadLevelAfterDelay(float delay)
	{
		yield return new WaitForSeconds(delay);
		Application.LoadLevel(NewLevelString);
	}
}
```

Was handed out our first assessment 