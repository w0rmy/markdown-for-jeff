# Week 01

I picked the VR module. The reason for this is that I currently am playing around with 360 video footage and Adobe Premiere. Have been doing this now for a while with my drone footage and and Go-Pro but like the idea of being 'In' the video as it offers with VR.

Have teamed up with Sam. Both of us have an Interest in Airsoft and can use that as a basis to work off.

Week one didnt involve too much. I had to [download and install unity](https://www.unity3d.com "Unity's Homepage") and then play inside it, get the basic handling of the interface and what things do.

Hardest thing I found was moving the view around with the mouse. Its rather different to the CAD and other spacial software I use, so this has been the hardest thing to get my head around.



## Links

* [Download and install unity](https://www.unity3d.com "Unity's Homepage")